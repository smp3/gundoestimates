package za.co.Gundo.estimator.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.Gundo.estimator.entities.CompanyDetail;
import za.co.Gundo.estimator.entities.Product;
import za.co.Gundo.estimator.repository.CompanyDetailRepository;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
public class CompanyDetailController {

    @Autowired
    private CompanyDetailRepository companyDetailRepository;
    @GetMapping("/company/detail")
    public ResponseEntity<?> addProducts(){
        List<CompanyDetail> all = companyDetailRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(all);

    }
}
