package za.co.Gundo.estimator.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.Gundo.estimator.entities.Product;
import za.co.Gundo.estimator.repository.ProductRepository;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
public class ProductController {
    @Autowired
    ProductRepository productRepository;

    @PostMapping("/add/product")
    public ResponseEntity<?> addProducts(@RequestBody @Valid Product product){
        @Valid Product save = productRepository.save(product);
        return ResponseEntity.status(HttpStatus.OK).body(save);

    }
    @GetMapping("/products")
    public ResponseEntity<?> getProducts(){
        List<Product> all = productRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(all);

    }
    @GetMapping("/products/{category}")
    public ResponseEntity<?> getProducts(@PathVariable String category){
        List<Product> all = productRepository.findByProductId(category);
        return ResponseEntity.status(HttpStatus.OK).body(all);

    }
}
