package za.co.Gundo.estimator.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import za.co.Gundo.estimator.entities.Customer;
import za.co.Gundo.estimator.repository.CustomerRepository;

@RestController
@CrossOrigin
public class CustomerController {
    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping("/customer/{name}")
    public ResponseEntity<?> getCustomerByName(@PathVariable String name){
        Customer byName = customerRepository.findByName(name);
        return ResponseEntity.status(HttpStatus.OK).body(byName);

    }
}
