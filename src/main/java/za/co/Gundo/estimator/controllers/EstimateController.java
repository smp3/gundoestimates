package za.co.Gundo.estimator.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.Gundo.estimator.business.EstimateBusinessService;
import za.co.Gundo.estimator.dtos.EstimateDTO;

import javax.validation.Valid;

@CrossOrigin
@RestController
public class EstimateController {
    @Autowired
    private EstimateBusinessService estimateBusinessService;

    @PostMapping("/estimate")
    public ResponseEntity<?> createEstimate(@Valid @RequestBody EstimateDTO estimateDTO)
    {
        return estimateBusinessService.createEstimate(estimateDTO);

    }
    @GetMapping("/estimates")
    public ResponseEntity<?> findAllEstimates()
    {
        return estimateBusinessService.getAllEstimates();

    }
    @GetMapping("/estimate/{quoteNumber}")
    public ResponseEntity<?> findAllEstimates(@PathVariable String quoteNumber)
    {
        return estimateBusinessService.getEstimateByQuoteNumber(quoteNumber);
    }
}
