package za.co.Gundo.estimator.dtos;

import com.sun.xml.internal.ws.api.ha.StickyFeature;

public class Totals {
    private String subtotal;
    private String grandTotal;

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }
}
