package za.co.Gundo.estimator.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import za.co.Gundo.estimator.entities.Product;
import za.co.Gundo.estimator.repository.ProductRepository;

import java.util.List;

@Service
public class ProductBusinessService {
    @Autowired
    private ProductRepository productRepository;

    public ResponseEntity<?> getProductsByCategory(String category){
        try {
            List<Product> byCategory = productRepository.findByProductId(category);
            return ResponseEntity.ok(byCategory);
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something went wrong");
        }

    }
}
