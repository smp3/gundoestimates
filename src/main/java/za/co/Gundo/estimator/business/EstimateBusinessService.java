package za.co.Gundo.estimator.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import za.co.Gundo.estimator.dtos.EstimateDTO;
import za.co.Gundo.estimator.dtos.ProductDTO;
import za.co.Gundo.estimator.dtos.Totals;
import za.co.Gundo.estimator.entities.CompanyDetail;
import za.co.Gundo.estimator.entities.Customer;
import za.co.Gundo.estimator.entities.Estimate;
import za.co.Gundo.estimator.entities.Product;
import za.co.Gundo.estimator.repository.*;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

@Service
public class EstimateBusinessService {

    @Autowired
    private EstimateRepository estimateRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CompanyDetailRepository companyDetailRepository;
    @Autowired
    private VatRepository vatRepository;
    private Logger logger = LoggerFactory.getLogger(EstimateBusinessService.class);
    public ResponseEntity<?> createEstimate(EstimateDTO estimateDTO){
        logger.info("Creating estimate.......");
        try {
            Estimate estimate = new Estimate();
            String companyName = estimateDTO.getCompanyName();
            double vatRate = Double.parseDouble(vatRepository.findById("2019").get().getRate());

            CompanyDetail companyDetail = companyDetailRepository.findByName(companyName);
            Optional<Customer> customer = customerRepository.findById(estimateDTO.getCustomerId());

                List<ProductDTO> productIds = estimateDTO.getProducts();
                Totals totals = calculateEstimateTotals(estimateDTO.getProducts());
                estimate.getCustomers().add(customer.get());
                customer.get().getEstimates().add(estimate);
                for (int i = 0; i < productIds.size(); i++) {
                    Optional<Product> product = productRepository.findById(productIds.get(i).getProductId());
                    if (!product.equals(Optional.empty())) {
                        estimate.setCompanyDetail(companyDetail);

                        estimate.getProducts().add(product.get());
                        estimate.setQuotationNumber(generateReference(product.get().getCategory()));
                        product.get().getEstimate().add(estimate);

                        estimate.setSubtotal(totals.getSubtotal());
                        estimate.setGrandTotal(totals.getGrandTotal());

                    }
                }
                Estimate save = estimateRepository.save(estimate);
                return ResponseEntity.ok(save);

        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something went wrong");

        }

    }
    public ResponseEntity<?> getAllEstimates(){
        logger.info("Getting all estimates");
        try {
            List<Estimate> estimates = estimateRepository.findAll();
            return ResponseEntity.ok(estimates);
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something went wrong");
        }
    }
    public ResponseEntity<?> getEstimateByQuoteNumber(String quoteNumber){
        logger.info("Getting all estimates by quote number");
        try {
            List<Estimate> estimates = estimateRepository.findByQuotationNumber(quoteNumber);
            logger.info("estimates by quotnumber",estimates);
            return ResponseEntity.ok(estimates);
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something went wrong");
        }
    }
    private String generateReference(String name) {
        //Check for name.
        String date = LocalDate.now().toString();
        Random randomInt = new Random();
        int random = randomInt.nextInt(1000);
        String system = "GUND";
        String string = UUID.randomUUID().toString().substring(0, 3);
        String reference = system + date + name + random + string;
        return reference;
    }

    private Totals calculateEstimateTotals(List <ProductDTO> productDTOS){
        double subtotal = 0;
        double grandtotal = 0;
        double vatRate = 0.15;
        for (int i = 0; i< productDTOS.size(); i ++){
            double price = Double.parseDouble(productDTOS.get(i).getPrice());
            double quantity = Double.parseDouble(productDTOS.get(i).getQuantity());
            subtotal += price*quantity;
        }
        grandtotal = subtotal + (subtotal*vatRate);
        Totals totals = new Totals();
        totals.setGrandTotal(String.valueOf(grandtotal));
        totals.setSubtotal(String.valueOf(subtotal));
        return totals;
    }
}
