package za.co.Gundo.estimator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.Gundo.estimator.entities.Customer;

public interface CustomerRepository extends JpaRepository<Customer, String> {
    Customer findByName(String name);
}
