package za.co.Gundo.estimator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.Gundo.estimator.entities.Product;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
    List<Product> findByProductId(String category);
}
