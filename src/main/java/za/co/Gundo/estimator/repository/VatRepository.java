package za.co.Gundo.estimator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.Gundo.estimator.entities.VAT;

public interface VatRepository extends JpaRepository<VAT, String> {

}
