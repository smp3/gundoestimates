package za.co.Gundo.estimator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.Gundo.estimator.entities.Estimate;

import java.util.List;

public interface EstimateRepository extends JpaRepository<Estimate, String> {

    List<Estimate> findByQuotationNumber(String quoteNumber);
}
