package za.co.Gundo.estimator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.Gundo.estimator.entities.CompanyDetail;

public interface CompanyDetailRepository extends JpaRepository<CompanyDetail, String> {
    CompanyDetail findByName(String companyName);

}
